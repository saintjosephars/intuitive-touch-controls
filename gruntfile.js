module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    'dev/js/scripts.js'
                ],
                dest: 'dev/js/build/scripts.production.js'
            }
        },
        uglify: {
            build: {
                src: 'dev/js/build/scripts.production.js',
                dest: 'compiled/js/production.min.js'
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    sourcemap: 'none'
                },
                files: {
                    //'dev/sass/_whp_styles.scss': '../live-share-files/whirlpool/home-and-category-pages/styles.css',       // 'destination': 'source'
                    'dev/raw-html-blocks/include/hero.css': 'dev/raw-html-blocks/sass/hero_block.scss',       // 'destination': 'source'
                    'dev/raw-html-blocks/include/comparison.css': 'dev/raw-html-blocks/sass/comparison.scss',       // 'destination': 'source'
                    'compiled/css/production.css': 'dev/sass/style.scss',       // 'destination': 'source'
                }
            }
        },
        include_file: {
            default_options: {
                cwd: 'dev/raw-html-blocks/build/',
                src: ['hero-block.html','comparison-chart.html'],
                dest: 'dev/raw-html-blocks/'
            }
        },
        watch: {
            scripts: {
                files: ['dev/js/*.js','dev/sass/*.scss','dev/raw-html-blocks/sass/*.scss','../live-share-files/whirlpool/home-and-category-pages/*.css','dev/raw-html-blocks/include/*.js','dev/raw-html-blocks/build/*'],
                tasks: ['concat', 'uglify', 'sass','include_file'],
                options: {
                    spawn: false,
                },
            } 
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat'); // Concatenation
    grunt.loadNpmTasks('grunt-contrib-uglify');   // Minification
    grunt.loadNpmTasks('grunt-contrib-sass');      // Sass
    grunt.loadNpmTasks('grunt-include-file');      // Include file
    grunt.loadNpmTasks('grunt-contrib-watch');  // Grunt watch

    // Default tasks
    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'include_file']);

};