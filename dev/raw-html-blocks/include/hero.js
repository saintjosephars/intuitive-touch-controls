$( document ).ready(function() {      
    
    function showTitle() {
        var heroIntroductionHeader = $('.intuitive-touch-hero-header');
        var heroIntroductionTitle = $('.hero__introduction-title');

        heroIntroductionHeader.addClass('initial');
        heroIntroductionTitle.addClass('initial');
    }

    function revealContent() {
        var intContainer = $('#intuitive-touch-hero-container');
        var heroIntroductionHeader = $('.intuitive-touch-hero-header');
        var heroIntroductionTitle = $('.hero__introduction-title');
        var heroIntroductionBody = $('.hero__introduction-body');
        var optionList = $('.option__list');
        var heroVidBlack = $('#vid_poster');

        intContainer.addClass('reveal');
        heroIntroductionHeader.addClass('reveal').removeClass('initial');
        heroIntroductionTitle.addClass('reveal').removeClass('initial');
        heroIntroductionBody.addClass('reveal');
        optionList.addClass('reveal');
        heroVidBlack.remove();
    }

    var width = $(window).width();

    if (width < 760) {
        $('#itc-vid').remove();

        setTimeout(function(){
            showTitle();
        }, 200);
        setTimeout(function(){
            revealContent();
        }, 4000);
    } else {
        var vid = document.getElementById('itc-vid');

        vid.play();

        vid.addEventListener('canplay', function() { 
            setTimeout(function(){
                showTitle();
            }, 100);
        });
        vid.addEventListener('ended', function() {
            setTimeout(function(){
                revealContent();
            }, 300);
        });
    }

});

var tabnav = document.querySelectorAll('.option__list-item');

var tabLinks = new Array();
var contentDivs = new Array();
var overlayDivs = new Array();

for ( var i = 0; i < tabnav.length; i++ ) {
    var tabLink = tabnav[i];
    var id = getHash( tabLink.getAttribute('href') );
    var overlay = tabLink.dataset.hash;
    tabLinks[id] = tabLink;
    contentDivs[id] = document.getElementById( id );
    overlayDivs[id] = document.getElementById( overlay );
}

// Assign onclick events to the tab links, and
// highlight the first tab
var i = 0;

for ( var id in tabLinks ) {
    tabLinks[id].onclick = showTab;
    tabLinks[id].onfocus = function() { this.blur() };
    /*if ( i == 0 ) {
        tabLinks[id].className = 'option__list-item active';
        contentDivs[id].className = 'option__content active';
        overlayDivs[id].className = 'console__overlay active';
    }*/
    i++;
}

// Hide all content divs except the first
/*var i = 0;

for ( var id in contentDivs ) {
    if ( i != 0 ) {
        contentDivs[id].className = 'option__content';
        overlayDivs[id].className = 'console__overlay';
    }
    i++;
}*/

function showTab() {
    var intContainer = $('#intuitive-touch-hero-container');
    var heroIntroduction = $('.hero__introduction');
    var featureTitle = $('.feature__title');
    var optionList = $('.option__list');
    var featurePanel = $('#intuitive-touch-panel');
    var featureConsole = $('#intuitive-touch-console');
    var heroIntroBody = $('.hero__introduction-body');

    var selectedId = getHash( this.getAttribute('href') );

    //Show the final states
    /*if( intContainer.hasClass('reveal') ) {
            intContainer.removeClass('reveal');
    }*/
    if(! intContainer.hasClass('active') ) {
        intContainer.addClass('active');
    }
    if(! heroIntroduction.hasClass('inactive') ) {
        heroIntroduction.addClass('inactive');
    }
    if(! optionList.hasClass('active') ) {
            optionList.fadeOut(500).delay(400).fadeIn(100).addClass('active');
    }
    if( optionList.hasClass('reveal') ) {
            optionList.removeClass('reveal');
    }
    if( heroIntroBody.hasClass('reveal') ) {
            heroIntroBody.removeClass('reveal');
    }
    setTimeout(function(){
        if(! featureTitle.hasClass('active') ) {
            featureTitle.addClass('active');
        }
        if(! featurePanel.hasClass('active') ) {
            featurePanel.addClass('active');
        }
        if(! featureConsole.hasClass('active') ) {
            featureConsole.addClass('active');
        }
    }, 1000);

    // Highlight the selected tab, and dim all others.
    // Also show the selected content div, and hide all others.
    for ( var id in contentDivs ) {
        if ( id == selectedId ) {
          tabLinks[id].className = 'option__list-item active';
          contentDivs[id].className = 'option__content active';
          overlayDivs[id].className = 'console__overlay active';
        } else {
          tabLinks[id].className = 'option__list-item';
          contentDivs[id].className = 'option__content';
          overlayDivs[id].className = 'console__overlay';
        }
    }

    // Stop the browser following the link
    return false;
}

function getHash( url ) {
      var hashPos = url.lastIndexOf ( '#' );
      return url.substring( hashPos + 1 );
}