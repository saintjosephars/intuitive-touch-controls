$(document).ready(function(){
    $('.color__selectable').click(function(){
        var tab_id = $(this).attr('data-hash');

        $('.color__selectable').removeClass('selected');
        $('.product__image.selectable').removeClass('selected');

        $(this).addClass('selected');
        $("#"+tab_id).addClass('selected');
    })
    
    //var width = $(document).width();
    var mql = window.matchMedia('all and (max-width: 767px)');
    var washerChart = $('#washer_chart');
    var productTable = $('.products-columns');
    var productColumn = $('.product');
    var swiper;
    var swiperArgs = {
        speed: 400,
        spaceBetween: 0,
        parallax: false,
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: false,
        pagination: '.swiper-pagination',
        paginationType: 'bullets',
        prevButton: '.swiper-button-prev',
        nextButton: '.swiper-button-next',
        preventClicks: true,
        preventClicksPropagation: true
    };

    if(mql.matches){
        washerChart.addClass('swiper-container');
        productTable.addClass('swiper-wrapper');
        productColumn.addClass('swiper-slide');
        washerChart.prepend('<div class="swiper-pagination"></div>');
        washerChart.prepend('<div class="swiper-button-prev swiper-button-black"></div>');
        washerChart.prepend('<div class="swiper-button-next swiper-button-black"></div>');
        swiper = new Swiper('.swiper-container', swiperArgs);
        
        console.log('Swiper active, ' + mql);
    }

    $(window).resize(function() {
        //var width = $(document).width();
        var mql = window.matchMedia('all and (max-width: 767px)');
        var mqlarge = window.matchMedia('all and (min-width: 768px)');

        if( mql.matches && !swiper){

            console.log('Swiper active, ' + mql);

            washerChart.addClass('swiper-container');
            productTable.addClass('swiper-wrapper');
            productColumn.addClass('swiper-slide');
            washerChart.prepend('<div class="swiper-pagination"></div>');
            washerChart.prepend('<div class="swiper-button-prev swiper-button-black"></div>');
            washerChart.prepend('<div class="swiper-button-next swiper-button-black"></div>');
            swiper = new Swiper('.swiper-container', swiperArgs);     

        } else if( mqlarge.matches ){
            if(swiper){
                console.log('Swiper deactivated, ' + mqlarge);

                swiper.destroy(true,true);
                swiper = undefined;
                washerChart.removeClass('swiper-container');
                washerChart.removeClass('swiper-container-horizontal');
                productTable.removeClass('swiper-wrapper');
                productColumn.removeClass('swiper-slide');
                productColumn.removeClass('swiper-slide-active');
                productColumn.removeClass('swiper-slide-next');
                $('.swiper-pagination').remove();
                $('.swiper-button-prev').remove();
                $('.swiper-button-next').remove();
                $('.swiper-slide-duplicate').remove();
            }                    
        }
    });
})